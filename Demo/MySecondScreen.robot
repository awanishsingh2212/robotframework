*** Settings ***
Library           SeleniumLibrary
Test Setup    Open Browser    ${URL}         ${browser}
Test Teardown    Close Browser
*** Variables ***
${URL}            https://opensource-demo.orangehrmlive.com/
${browser}        Chrome


*** Test Cases ***

 
Login Test
    [Tags]    Smoke
    Maximize Browser Window
    Enter Username
    Enter Password
    click on Login
    
      
    Log To Console    ${URL}  
    Should Contain    ${URL}    dashboard    
*** Keywords ***
 Enter Username
    Input Text    id:txtUsername    admin
 Enter Password
    Input Text    name:txtPassword  admin123
 click on Login
    Click Button    xpath://input[@value='LOGIN'] 
    
    
      
   